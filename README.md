### Kata of JSON parsing and Card View Interface Building ###

* Reddit JSON Parsing Kata
* 0.1
* [Video Tutorial](https://www.youtube.com/watch?v=AmX_f5hWfwk)

### How do I get set up? ###

* This project is tested on XCode 7.2
* Go to the project directory, and run "pod install" (in the terminal)
* Use XCode to open the workspace file.
* Everything should run out of the box.

### Who do I talk to? ###

* if you have any question, contact dsuyang@umich.edu