//
//  RedditViewController.m
//  Reddit
//
//  Created by Suyang Dong on 4/20/17.
//  Copyright © 2017 RippleArc. All rights reserved.
//

#import "RedditViewController.h"
#import "RedditPost.h"
#import "SVPullToRefresh.h"
#import <SDWebImage/UIImageView+WebCache.h>


@interface RedditViewController ()

@property (strong, nonatomic) NSMutableArray *dataSource;
@property (strong, nonatomic) NSString *nextURL;
@property (strong, nonatomic) NSString *currentURL;


@end

@implementation RedditViewController

NSString * const kRedditBaseURL = @"https://www.reddit.com/.json";
NSString * const kRedditURLFormat = @"https://www.reddit.com/r/redditdev/.json?limit=10&after=%@/";
NSString * const kRedditJsonListing = @"Listing";
NSString * const kRedditJsonData = @"data";
NSString * const kRedditJsonAfter = @"after";
NSString * const kRedditJsonChildren = @"children";
NSString * const kRedditThumbnail = @"thumbnail";
NSArray  * kRedditImageDefault;
NSString * const kRedditTitle = @"title";
NSString * const kRedditNumComments = @"num_comments";
NSString * const kRedditScore = @"score";
NSString * const kRedditName = @"subreddit_name_prefixed";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.allowsSelection = false;
    _dataSource = [NSMutableArray new];
    kRedditImageDefault = @[@"default", @"image", @"self"];
    __weak RedditViewController *weakSelf = self;

    
    // setup pull-to-refresh
    [self.tableView addPullToRefreshWithActionHandler:^{
        [weakSelf insertRowAtTop];
    }];
    
    // setup infinite scrolling
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        [weakSelf insertRowAtBottom];
    }];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidAppear:(BOOL)animated {
    [self.tableView triggerPullToRefresh];
}

#pragma mark - SVPullToRefresh


- (void)insertRowAtTop {
 
    _currentURL = kRedditBaseURL;
    _nextURL = kRedditBaseURL;
//    self.tableView.estimatedRowHeight = 400;
  //  self.tableView.rowHeight = UITableViewAutomaticDimension;
    [self retrieveDataSource: [NSURL URLWithString:_nextURL] refresh:true];
}


- (void)insertRowAtBottom {
    __weak RedditViewController *weakSelf = self;

    if ([_currentURL isEqualToString:_nextURL] || _nextURL == nil) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.tableView.infiniteScrollingView stopAnimating];
        });
        return;
    } else {
        _currentURL = _nextURL;
        [self retrieveDataSource: [NSURL URLWithString:_nextURL] refresh:false];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row < [_dataSource count]) {
        RedditPost *post = [_dataSource objectAtIndex:indexPath.row];
        
        UIImageView *dummyImage = [UIImageView new];
        if (![kRedditImageDefault containsObject:post.thunbnailURL]) {
            [dummyImage sd_setImageWithURL:[NSURL URLWithString:post.thunbnailURL]
                              placeholderImage:[UIImage imageNamed:@"reddit"]];
        } else {
            [dummyImage setImage:[UIImage imageNamed:@"reddit"]];
        }

        return 48 + [self heightForLabel:post.title] + [self heightForImage:dummyImage.image];
    } else {
        return 0;
    }

}



-(CGFloat)heightForLabel:(NSString *)text
{
    CGRect labelRect = [text
                        boundingRectWithSize:CGSizeMake(self.tableView.bounds.size.width, 0)
                        options:NSStringDrawingUsesLineFragmentOrigin
                        attributes:@{
                                     NSFontAttributeName : [UIFont systemFontOfSize:17]
                                     }
                        context:nil];
    return labelRect.size.height+5;
}

-(CGFloat)heightForImage:(UIImage *)image
{
    return self.tableView.bounds.size.width * (image.size.height / image.size.width);
}

#pragma mark - Table view data source
- (void) retrieveDataSource:(NSURL *)url refresh:(BOOL)refresh
{
    __weak RedditViewController *weakSelf = self;
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:url
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error) {
                // handle response
                NSError *e;
                NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&e]];
                
                if (e == nil) {
                    [weakSelf updateDataSource:dict refresh:refresh];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [weakSelf.tableView reloadData];
                        
                    });
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (refresh)
                        [weakSelf.tableView.pullToRefreshView stopAnimating];
                    else
                        [weakSelf.tableView.infiniteScrollingView stopAnimating];
                });

                
            }] resume];
    }

- (void) updateDataSource:(NSDictionary *) dict refresh:(BOOL)refresh
{
    NSDictionary *data = [dict objectForKey:kRedditJsonData];
    if (data != nil) {
       
        
        NSArray *children = [data objectForKey:kRedditJsonChildren];
        NSMutableArray *posts = [NSMutableArray arrayWithCapacity:children.count];
        if (posts != nil) {
            for (NSDictionary *child in children)
            {
                RedditPost *post = [RedditPost new];
                post.thunbnailURL = [[child objectForKey:kRedditJsonData] objectForKey:kRedditThumbnail];
                post.title = [[child objectForKey:kRedditJsonData]  objectForKey:kRedditTitle];
                post.redditName = [[child objectForKey:kRedditJsonData]  objectForKey:kRedditName];
                post.score = [[[child objectForKey:kRedditJsonData]  objectForKey:kRedditScore] intValue];
                post.numComments = [[[child objectForKey:kRedditJsonData]  objectForKey:kRedditNumComments] intValue];
                [posts addObject:post];
            }
            if (refresh)
                _dataSource = posts;
            else
                [_dataSource addObjectsFromArray:posts];
            
        }
        
        
        NSString *next = [data objectForKey:kRedditJsonAfter];
        if (next != nil) {
            _nextURL = [NSString stringWithFormat:kRedditURLFormat, next];
        } else {
            _nextURL = nil;
        }
        
    }
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_dataSource count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RedditPostCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RedditCell" forIndexPath:indexPath];

    if (indexPath.row < [_dataSource count]) {
        RedditPost *post = [_dataSource objectAtIndex:indexPath.row];
        cell.lblName.text = post.redditName;
        cell.lblTitle.text = post.title;
        cell.lblTitle.numberOfLines = 0;

        
        // override layout height constraint.
        cell.lblTitleHeightConstraint.constant = [self heightForLabel:post.title];

        cell.lblScore.text = [NSString stringWithFormat:@"%d",post.score];
        cell.lblScore.textAlignment = NSTextAlignmentCenter;
        cell.lblNumComment.text = [NSString stringWithFormat:@"%d",post.numComments];
        
        
        if (![kRedditImageDefault containsObject:post.thunbnailURL]) {
            [cell.coverView sd_setImageWithURL:[NSURL URLWithString:post.thunbnailURL]
                          placeholderImage:[UIImage imageNamed:@"reddit"]];
        } else {
            [cell.coverView setImage:[UIImage imageNamed:@"reddit"]];
        }
        [cell.coverView setFrame:CGRectMake(cell.coverView.frame.origin.x, cell.coverView.frame.origin.y,
                                            self.tableView.bounds.size.width,
                                            [self heightForImage:cell.coverView.image])];

   
    }
    
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

@implementation RedditPostCell

@end
