//
//  RedditPost.h
//  Reddit
//
//  Created by Suyang Dong on 4/20/17.
//  Copyright © 2017 RippleArc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface RedditPost : NSObject
@property (strong, nonatomic) NSString *thunbnailURL;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *redditName;
@property (assign) int score;
@property (assign) int numComments;

@end




