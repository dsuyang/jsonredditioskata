//
//  AppDelegate.h
//  Reddit
//
//  Created by Suyang Dong on 4/20/17.
//  Copyright © 2017 RippleArc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

