//
//  RedditViewController.h
//  Reddit
//
//  Created by Suyang Dong on 4/20/17.
//  Copyright © 2017 RippleArc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RedditViewController : UITableViewController

@end

@interface RedditPostCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *coverView;
@property (weak, nonatomic) IBOutlet UILabel *lblScore;
@property (weak, nonatomic) IBOutlet UILabel *lblNumComment;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblTitleHeightConstraint;

@end
